import { Component } from "rainbowui-desktop-core";
import { Util } from 'rainbow-desktop-tools';
import MarkdownIt from 'markdown-it'
import MdEditor from 'react-markdown-editor-lite'
import 'react-markdown-editor-lite/lib/index.css';
import PropTypes from 'prop-types';

export default class MarkDownEditor extends Component {

    constructor(props) {
        super(props);
        this.editor = null;
    }

    /** Useage
     * <UIMarkDownEditor id="editDescEditor" style={{height:'800px'}} model={this.state.ApiModule} htmlProperty="HTML" mdProperty="MD"  />
     */
    
    render() {
        const mdParser = new MarkdownIt(/* Markdown-it options */);

        return (
            <MdEditor
                id={this.props.id}
                style={this.props.style}
                value={this.getValue()}
                renderHTML={(text) => mdParser.render(text)}
                onChange={this.handleEditorChange.bind(this)}
            />
        );
    }

    handleEditorChange({html, text}) {    
        // console.log('handleEditorChange', html, text);
        const model = this.props.model;
        const htmlProperty = this.props.htmlProperty;
        const mdProperty = this.props.mdProperty;
        let htmlInputValue = null;
        let mdInputValue = null;
        if (model && htmlProperty) {
            model[htmlProperty] = html;
        }

        if (model && mdProperty) {
            model[mdProperty] = text;
        }
    }

    getValue() {
        const model = this.props.model;
        const htmlProperty = this.props.htmlProperty;
        const mdProperty = this.props.mdProperty;
        let htmlInputValue = null;
        let mdInputValue = null;
        if (model && htmlProperty) {
            htmlInputValue = model[htmlProperty];
        }

        if (model && mdProperty) {
            mdInputValue = model[mdProperty];
        }

        return mdInputValue;
    }

    
};


/**
 * MarkDownEditor component prop types
 */
MarkDownEditor.propTypes = {
    isInline: PropTypes.oneOf([PropTypes.string, PropTypes.bool]),
    model: PropTypes.object,
    style: PropTypes.object,
    property: PropTypes.string,
    hideToolbar: PropTypes.oneOf([PropTypes.string, PropTypes.bool]),
    errorMessage: PropTypes.string,
    disabled: PropTypes.oneOf([PropTypes.string, PropTypes.bool]),
    toolbarCollapse: PropTypes.oneOf([PropTypes.string, PropTypes.bool]),
    showWordCount: PropTypes.oneOf([PropTypes.string, PropTypes.bool]),
    showCharCount: PropTypes.oneOf([PropTypes.string, PropTypes.bool]),
    countHTML: PropTypes.oneOf([PropTypes.string, PropTypes.bool]),
    maxWordCount: PropTypes.number,
    maxCharCount: PropTypes.number,
    height: PropTypes.string
};

/**
 * Get MarkDownEditor component default props
 */
MarkDownEditor.defaultProps = {
    isInline: false,
    hideToolbar: false,
    style: {},
    disabled: false,
    toolbarCollapse: false,
    showWordCount: false,
    showCharCount: false,
    countHTML: false,
    maxWordCount: -1,
    maxCharCount: -1,
    height: '150px'
};