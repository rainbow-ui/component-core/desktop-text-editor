import { Component } from "rainbowui-desktop-core";
import { Util } from 'rainbow-desktop-tools';
import PropTypes from 'prop-types';

export default class TextEditor extends Component {

    constructor(props) {
        super(props);
        this.editor = null;
    }

    render() {
        return (
            <textarea id={this.props.id} rows="10" cols="80">{this.getOutputValue()}</textarea>
        );
    }

    getOutputValue() {
        let inputValue = null;
        const model = this.props.model;
        const property = this.props.property;
        if (model && property) {
            inputValue = model[property];
        }
        return inputValue;
    }


    componentDidMount() {
        const { id } = this.props;
        let editor2 = CKEDITOR.instances[id];
        if (editor2) { 
            editor2.destroy();//销毁编辑器
            CKEDITOR.remove(editor2);
            editor2 = null;
        }
        this.initTextEditor();
    }

    componentDidUpdate() {
        super.componentDidUpdate();
        
        let _self = this;
        const { id } = this.props;
        let inputValue = null;
        const model = this.props.model;
        const property = this.props.property;
        if (model && property) {
            inputValue = model[property];
        }
        if (CKEDITOR.instances[id] && inputValue) {
            CKEDITOR.instances[id].setData(inputValue);
            let iframe = $('iframe.cke_wysiwyg_frame');
            if (iframe && iframe.contents().find('body').length > 0) {
                iframe.contents().find('body').html(inputValue);
            } else {
                setTimeout(() => {
                    iframe.contents().find('body').html(inputValue);
                }, 500)
                
            }
            CKEDITOR.instances[id].readOnly = Util.parseBool(_self.props.disabled);
        }
    }
    initTextEditor() {
        const _self = this;
        const { id } = this.props;
        if (Util.parseBool(_self.props.isInline)) {
            this.editor = CKEDITOR.inline(id,
                {
                    readOnly: Util.parseBool(_self.props.disabled),
                    wordcount: {
                        showCharCount: Util.parseBool(_self.props.showCharCount),
                        showWordCount: Util.parseBool(_self.props.showWordCount),
                        countHTML: Util.parseBool(_self.props.countHTML),
                        maxWordCount: _self.props.maxWordCount,
                        maxCharCount: _self.props.maxCharCount,
                    }

                }
            );
            this.editor.on('change', function (e) {
                _self.setEditorValue(e);
            })
        } else {
            this.editor = CKEDITOR.replace(id,
                {
                    toolbar: Util.parseBool(_self.props.hideToolbar) ? [[""]] : null,
                    readOnly: Util.parseBool(_self.props.disabled),
                    toolbarCanCollapse: Util.parseBool(_self.props.toolbarCollapse),
                    toolbarStartupExpanded: false,
                    removePlugins:"elementspath",
                    height: this.props.height,
                    wordcount: {
                        showCharCount: Util.parseBool(_self.props.showCharCount),
                        showWordCount: Util.parseBool(_self.props.showWordCount),
                        countHTML: Util.parseBool(_self.props.countHTML),
                        maxWordCount: _self.props.maxWordCount,
                        maxCharCount: _self.props.maxCharCount,
                    }

                }
            );
            this.editor.on('change', function (e) {
                _self.setEditorValue(e);
            });
        }
    }

    setEditorValue(e){
        const model = this.props.model;
        const property = this.props.property;
        if (model && property) {
            model[property] = e.editor.getData();
        }
    }
};


/**
 * TextEditor component prop types
 */
TextEditor.propTypes = {
    isInline: PropTypes.oneOf([PropTypes.string, PropTypes.bool]),
    model: PropTypes.object,
    property: PropTypes.string,
    hideToolbar: PropTypes.oneOf([PropTypes.string, PropTypes.bool]),
    errorMessage: PropTypes.string,
    disabled: PropTypes.oneOf([PropTypes.string, PropTypes.bool]),
    toolbarCollapse: PropTypes.oneOf([PropTypes.string, PropTypes.bool]),
    showWordCount: PropTypes.oneOf([PropTypes.string, PropTypes.bool]),
    showCharCount: PropTypes.oneOf([PropTypes.string, PropTypes.bool]),
    countHTML: PropTypes.oneOf([PropTypes.string, PropTypes.bool]),
    maxWordCount: PropTypes.number,
    maxCharCount: PropTypes.number,
    height: PropTypes.string
};

/**
 * Get TextEditor component default props
 */
TextEditor.defaultProps = {
    isInline: false,
    hideToolbar: false,
    disabled: false,
    toolbarCollapse: false,
    showWordCount: false,
    showCharCount: false,
    countHTML: false,
    maxWordCount: -1,
    maxCharCount: -1,
    height: '150px'
};