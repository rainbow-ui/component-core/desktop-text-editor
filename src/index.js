'use strict';

module.exports = {
    UITextEditor: require('./component/TextEditor'),
    UIMarkDownEditor: require('./component/MarkDownEditor')
};